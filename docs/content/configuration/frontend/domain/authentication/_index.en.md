---
title: Authentication
description: Authentication domain settings
weight: 20
tags:
    - configuration
    - frontend
    - Administration UI
    - domain
    - authentication
    - local authentication
    - anonymous
    - Facebook
    - GitHub
    - GitLab
    - Google
    - Twitter
    - X
    - SSO
    - Single Sign-On
seeAlso:
    - /configuration/frontend/domain
    - /configuration/idps/facebook
    - /configuration/idps/github
    - /configuration/idps/gitlab
    - /configuration/idps/google
    - /configuration/idps/twitter
---

The `Authentication` tab allows to configure commenter authentication options for the domain.

<!--more-->

These options include:

* Anonymous comments
* Local (username-and-password-based)
* Social login via external identity providers:
    * [Facebook](/configuration/idps/facebook)
    * [GitHub](/configuration/idps/github)
    * [GitLab](/configuration/idps/gitlab)
    * [Google](/configuration/idps/google)
    * [Twitter/X](/configuration/idps/twitter)
* [Single Sign-On](sso)
