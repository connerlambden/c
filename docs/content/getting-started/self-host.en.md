---
title: Self-hosting
description: How to start self-hosting Comentario
weight: 100
tags:
    - getting started
    - self-hosting
    - installation
---

To self-host a Comentario instance, you'll need two components: a database and a network-attached server machine.

<!--more-->

## Database

Have a look at the [](installation/requirements) document to learn about the database component.

## Server

There's a number of options when it comes to running the server, depending on your demands and network architecture. Please proceed to the [](/installation) page to choose the one that suits you.
